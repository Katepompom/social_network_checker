### Импорт библиотек
import requests
from requests.exceptions import Timeout
import random
import string
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import threading
import pandas as pd
from multiprocessing.dummy import Pool as ThreadPool 
from time import sleep
from selenium.webdriver import ActionChains
import csv

### Вспомогательные процедуры
def generate_names(num=100, name_size=10):
    """
    Генератор имен из английских букв и точек (для тестов)
    """
    res = []
    letters = string.ascii_letters + string.digits + '.'
    for name_i in range(num):
        res.append(''.join(random.choice(letters) for i in range(name_size)))
    return res

### Процедуры для получения результатов с использованием request

# Описание реализации: Для каждого имени из списка делается попытка получить страницу профиля на facebook и instagram. Если возвращается ответ 404, тогда считается, что такого профиля не существует и имя свободно. В противном случае оно занято.

def check_username(username):
    """
    
    Функция проверки доступности одного имени на facebook и instagram.
    
    Переменные:
    username - имя пользователя, наличие которого надо проверить на facebook и instagram.
    
    Возращает словарь, где:
    request - имя пользователя, которое проверяется
    facebook url - адрес профиля на facebook
    facebook status - статус проверки наличия профиля на facebook
    instagram url - адрес профиля на instagram
    instagram status - статус проверки наличия профиля на instagram
   
    """
    f_url = "https://mbasic.facebook.com/" + username
    i_url = "https://www.instagram.com/" + username
    f_status = ''
    i_status = ''
    try:
        f_response = requests.get(f_url, timeout=2)
        if f_response:
            f_status = 'username exist'
        else:
            f_status = 'username not exist'
    except Timeout:
        f_status = f'The request for {username} timed out'
    try:
        i_response = requests.get(i_url, timeout=3)
        if i_response:
            i_status = 'username exist'
        else:
            i_status = 'username not exist'
    except Timeout:
        i_status = f'The request for {username} timed out'
    return {'request':username, 'facebook url':"https://facebook.com/" + username, 'facebook status':f_status, 'instagram url':i_url, 'instagram status':i_status}

def check_username_list(username_list):
    """
    
    Функция проверки списка имен пользователей. Запускает параллельно несколько потоков выполнения функций 
    check_username для каждого имени из списка для проверки.
    
    Переменные:
    username_list - список имен пользователей, наличие которых надо проверить на facebook и instagram.
    
    Возращает данные в виде pandas.dataframe, где: 
    request - имя пользователя, которое проверяется
    facebook url - адрес профиля на facebook
    facebook status - статус проверки наличия профиля на facebook
    instagram url - адрес профиля на instagram
    instagram status - статус проверки наличия профиля в instagram
       
    """
    pool = ThreadPool(4)
    results = pool.map(check_username, username_list)
    checking_result = pd.DataFrame(results)
    return checking_result

### Процедуры для получения результатов с использованием selenium

# Описание реализации: Путем имитации действий пользователя на сайтах instagram и facebook, определеяет доступность имени на сайте. Для facebook имитирует процесс смены имени действующего пользователя через настройки профиля путем перебирания списка имен. Для instagram имитирует регистрацию пользователя и последовательную проверку доступности имени для регистрации по каждому имени из списка. Данная реализация разбита на два этапа:

# - Поиск доступных имен на facebook 
# - Поиск доступных имен в instagram

def check_username_list_facebook(username_list):
    """
    
    Функция проверки доступности имен пользователей на facebook по списку.
    
    Переменные:
    username_list - список имен пользователей, наличие которых надо проверить на facebook.
    
    Возращает данные в виде pandas.dataframe, где: 
    request - имя пользователя, которое проверяется
    facebook url - адрес профиля на facebook
    facebook status - статус проверки наличия профиля на facebook 
                        
    В качестве статуса проверки также может вернуть ошибку ввода имени, 
    если имя не соответствует стандартам имен на facebook
    
    """
    usr = # <Ваш логин facebook>
    pwd = # <Ваш пароль facebook>
    
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")
    driver = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)

#   вход в аккаунт на facebook  
    driver.get("https://www.facebook.com")
    
    elem = driver.find_element_by_id("email")
    elem.send_keys(usr)
    elem = driver.find_element_by_id("pass")
    elem.send_keys(pwd)
    elem.send_keys(Keys.RETURN)
    
    sleep(3)
    
#   переход в настройки пользователя для смены имени
    driver.get("https://www.facebook.com/settings?tab=account&section=username&view")
   
    sleep(3)

#   последовательный перебор имен по списку, заполнение поля username
#   и определение его доступности по полю результата проверки (status)
    
    username_field = driver.find_element_by_name("username")
    username_field.clear()
    
    result = []
    for name in username_list:
        username_field.send_keys(name)
        time.sleep(3)
        status = driver.find_element_by_class_name("fbSettingsUsernameStatus")
        try:
            result.append({"request":name, "facebook_url":"https://facebook.com/" + name, "facebook_status":status.text})
        except:
            result.append({"request":name, "facebook_url":"https://facebook.com/" + name, "facebook_status":"checking error"})
        username_field.clear()
    
    driver.close()
    
    return pd.DataFrame(result)

def check_usernames_instagram(username_list):
    """
    
    Функция проверки доступности имен пользователей в instagram по списку.
    
    Переменные:
    username_list - список имен пользователей, наличие которых надо проверить на facebook.
    
    Возращает данные в виде pandas.dataframe, где: 
    request - имя пользователя, которое проверяется
    instagram url - адрес профиля на instagram
    instagram status - статус проверки наличия профиля в instagram 
                        
    В качестве статуса проверки также может вернуть ошибку ввода имени, 
    если имя не соответствует стандартам имен в instagram
    
    """
    result = []
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")

#   вход на страницу регистрации нового аккаунта в instagram
    driver = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)
    driver.get('https://www.instagram.com/accounts/emailsignup/')

    sleep(5)

#   последовательный перебор имен по списку, заполнение поля username
#   и определение его доступности по полю результата проверки (alert_field)

    action_chains = ActionChains(driver)
    username_field = driver.find_element_by_name('username')
    
    for name in username_list:
        action_chains.move_to_element(username_field)
        username_field.send_keys(Keys.CONTROL, 'a')
        username_field.send_keys(name)
        sleep(1)

        username_field.send_keys(Keys.SHIFT, Keys.ENTER)
        sleep(5)

        try:
            alert_field = driver.find_element_by_id("ssfErrorAlert")
            if alert_field.text == 'This field is required.':
                result.append({"request":name, "instagram_url":"https://www.instagram.com/" + name, "instagram_status":'username available'})
            else:
                result.append({"request":name, "instagram_url":"https://www.instagram.com/" + name, "instagram_status":alert_field.text})
        except:
            result.append({"request":name, "instagram_url":"https://www.instagram.com/" + name, "instagram_status":'username available'})
            
    driver.close()
    return pd.DataFrame(result)

def check_usernamelist_selenium(username_list):
    result = []
    if isinstance(username_list, list) and len(username_list) != 0:
        facebook_result = check_username_list_facebook(username_list)
        instagram_result = check_usernames_instagram(username_list)
        result = pd.merge(facebook_result, instagram_result, on='request')
    else:
        print("Parameter error")
    return result